package com.example.samplea;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import com.example.samplea.entity.Book;
import com.example.samplea.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;

@RestClientTest(BookService.class)
class BookServiceTest {

    @Autowired
    private BookService service;

    @Autowired
    private MockRestServiceServer server;

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    void getBookByIsbnWhenCircuiteBreakIsOpenReturnBookFallBack() throws Exception {

        Book bookFallBack = new Book(100,"titleFallBack", "authorFallBack", "123");

        this.server.expect(requestTo("http://localhost:8181/recommended/isbn/123"))
                .andRespond(withSuccess(mapper.writeValueAsString(bookFallBack), MediaType.APPLICATION_JSON));

        Book book = this.service.bookByIsbn("123");

        assertThat(bookFallBack).isEqualTo(book);
    }
}
