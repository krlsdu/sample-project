CREATE TABLE books(
    id SERIAL,
    title text,
    author text,
    isbn text PRIMARY KEY
)