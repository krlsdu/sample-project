package com.example.sampleb.audit;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CustomEventRepository implements AuditEventRepository {

    @Autowired
    private AuditEventRepository inMemoryAuditEventRepository;

    AtomicReference<AuditEvent> lastEvent = new AtomicReference<>();

    @Override
    public void add(AuditEvent event) {
        lastEvent.set(event);
    }

    @Override
    public List<AuditEvent> find(String principal, Instant after, String type) {
        return inMemoryAuditEventRepository.find(principal, after, type);
    }

}