package com.example.samplea.repository;

import java.util.Collection;

import com.example.samplea.entity.Book;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {

    Book findByIsbn(String isbn);

    Collection<Book> findAll();

    Page<Book> findAll(Pageable pageable);

}