package com.example.sampleb.repository;

import java.util.Collection;

import com.example.sampleb.entity.Book;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {

    Book findByIsbn(String isbn);

    Collection<Book> findAll();

    Page<Book> findAll(Pageable pageable);

}