package com.example.sampleb.service;

import java.net.URI;

import com.example.sampleb.entity.Book;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;

@Service
public class BookService {

    private final RestTemplate restTemplate;

    private static final String SAMPLE_B = "sampleB";

    public BookService(RestTemplate rest) {
        this.restTemplate = rest;
    }

    // Observacao ao usar o fallbackMethod
    // https://resilience4j.readme.io/docs/getting-started-3#annotations
    // @CircuitBreaker(name = SAMPLE_A, fallbackMethod = "secundario")
    @CircuitBreaker(name = SAMPLE_B)
    @Retry(name = SAMPLE_B)
    public Book bookByIsbn(String isbn) {
        URI uri = URI.create("http://localhost:8090/recommended/isbn");

        return this.restTemplate.getForObject(uri, Book.class);
    }

    private Book secundario(Exception ex) {

        Book book = new Book();
        book.setAuthor("authorFallBack");
        book.setTitle("titleFallBack");
        return book;
    }

}